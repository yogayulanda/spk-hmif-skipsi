<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Mahasiswa extends Model
{
    protected $table = 'mahasiswa';
    protected $fillable = ['nama_mahasiswa', 'nim', 'kelas', 'jenis_kelamin', 'hasil_kriteria', 'ket_hasil'];

    public function KriteriaAspirasi()
    {
        return $this->belongsToMany(KriteriaAspirasi::class)->withPivot('nilai', 'nilai_bobot_kriteria')->withTimestamps();
    }

    public function nilai()
    {
        return $this->hasMany(NilaiAspirasi::class);
    }




    // public function aspirasi()
    // {
    //     $kali = 0;
    //     foreach ($this->KriteriaAspirasi as $aspirasi) {
    //         $kali = $aspirasi->pivot->nilai * $aspirasi->pivot->nilai_bobot_kriteria;
    //     }
    //     dd($kali);
    // }
}
