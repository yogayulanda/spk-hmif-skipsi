<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NilaiAspirasi extends Model
{
    protected $table = 'kriteria_aspirasi_mahasiswa';
    protected $fillable = ['mahasiswa_id', 'kriteria_aspirasi_id', 'nilai', 'nilai_bobot_kriteria'];
}
