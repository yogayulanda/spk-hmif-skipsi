<?php

namespace App\Http\Controllers;

use App\Mahasiswa;
use App\KriteriaAspirasi;
use App\NilaiAspirasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\VarDumper\Cloner\Data;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)

    {
        if ($request->has('cari')) {
            $data_mahasiswa = \App\Mahasiswa::where('nama_mahasiswa', 'LIKE', '%' . $request->cari . '%')->get();
        } else {
            $data_mahasiswa = \App\Mahasiswa::all();
        }

        return view('mahasiswa/index', ['data_mahasiswa' => $data_mahasiswa]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        \App\Mahasiswa::create($request->all());

        return redirect('/mahasiswa')->with('sukses', 'Data Berhasil Ditambahkan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mahasiswa = \App\Mahasiswa::find($id);
        return view('mahasiswa/edit', ['mahasiswa' => $mahasiswa]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mahasiswa = \App\Mahasiswa::find($id);
        $mahasiswa->update($request->all());
        return redirect('/mahasiswa')->with('sukses', 'Data Berhasil Di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $mahasiswa = \App\Mahasiswa::find($id);
        $mahasiswa->delete($mahasiswa);
        return redirect('/mahasiswa')->with('sukses', 'Data Berhasil Di Hapus');
    }

    public function profile($id)
    {
        $mahasiswa = \App\Mahasiswa::find($id);
        $tambah = KriteriaAspirasi::all();
        // dd($tambah);

        return view('/mahasiswa/profile', ['mahasiswa' => $mahasiswa], ['tambah' => $tambah]);
    }

    public function addnilai(Request $request, $idmahasiswa)
    {


        $mahasiswa = \App\Mahasiswa::find($idmahasiswa);
        $mahasiswa->KriteriaAspirasi()->attach($request->KriteriaAspirasi, ['nilai'  => $request->nilai, 'nilai_bobot_kriteria' => $request->nilai_bobot_kriteria]);

        return redirect('mahasiswa/' . $idmahasiswa . '/profile')->with('sukses', 'data');
    }

    public function deletenilai($idmahasiswa, $idkriteria)
    {


        $mahasiswa = \App\Mahasiswa::find($idmahasiswa);
        $mahasiswa->KriteriaAspirasi()->detach($idkriteria);
        // // dd($tambah);
        return redirect()->back()->with('sukses', 'Data Berhasil Dihapus');

        // return view('/mahasiswa/profile', ['mahasiswa' => $mahasiswa], ['tambah' => $tambah]);
    }
}
