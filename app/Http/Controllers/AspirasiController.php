<?php

namespace App\Http\Controllers;

use App\KriteriaAspirasi;
use App\Mahasiswa;
use App\NilaiAspirasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use mysqli;
use Symfony\Component\VarDumper\Cloner\Data;

class AspirasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)

    {
        if ($request->has('cari')) {
            $data_mahasiswa = \App\Mahasiswa::where('nama_mahasiswa', 'LIKE', '%' . $request->cari . '%')->get();
        } else {
            $data_mahasiswa = Mahasiswa::all();
            $kriteria = KriteriaAspirasi::all();
        }

        return view('aspirasi/index', compact('data_mahasiswa', 'kriteria'));
        // return view('aspirasi/index', ['data_mahasiswa' => $data_mahasiswa], ['kriteria' => $kriteria]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        \App\KriteriaAspirasi::create($request->all());
        return redirect('/aspirasi')->with('sukses', 'Data Berhasil Ditambahkan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kriteria = \App\KriteriaAspirasi::find($id);
        return view('aspirasi/edit', ['kriteria_aspirasi' => $kriteria]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mahasiswa = \App\KriteriaAspirasi::find($id);
        $mahasiswa->update($request->all());
        return redirect('/aspirasi')->with('sukses', 'Data Berhasil Di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $kriteria = \App\KriteriaAspirasi::find($id);
        $kriteria->delete($kriteria);
        return redirect('/aspirasi')->with('sukses', 'Data Berhasil Di Hapus');
    }

    public function hitungutility(Request $request)
    {
        if ($request->has('cari')) {
            $data_mahasiswa = \App\Mahasiswa::where('nama_mahasiswa', 'LIKE', '%' . $request->cari . '%')->get();
        } else {
            $data_mahasiswa = Mahasiswa::all();
            $kriteria = KriteriaAspirasi::all();
        }

        return view('aspirasi/hitungutility', compact('data_mahasiswa', 'kriteria'));
        // return view('aspirasi/index', ['data_mahasiswa' => $data_mahasiswa], ['kriteria' => $kriteria]);

    }

    public function smart(Request $request)
    {
        if ($request->has('cari')) {
            $data_mahasiswa = \App\Mahasiswa::where('nama_mahasiswa', 'LIKE', '%' . $request->cari . '%')->get();
        } else {




            $kriteria = KriteriaAspirasi::all();
            $nilai = NilaiAspirasi::where(['nilai']);
            $sum = KriteriaAspirasi::sum('bobot_kriteria');
            foreach (KriteriaAspirasi::all() as $k => $v) {
                //Normalisasi Bobot Kriteria
                $bobot[$v->id] = $v->bobot_kriteria / $sum;
            }

            // Ambil nilai id mahasiswa = nilai_bobot_kriteria

            $bobot = array();

            foreach (NilaiAspirasi::all() as $i => $v) {

                $bobot[$v->mahasiswa_id] = $v->nilai_bobot_kriteria;
            }
            //Normalisasi Bobot Kriteria

            // Ambil nilai id mahasiswa = nilai
            $users = NilaiAspirasi::with(['nilai'])->orderBy('mahasiswa_id', 'DESC')->get();
            dd($users);
            return response()->json(['data' => $users]);
            // Ambil nilai id mahasiswa = hasil sum 


            //-- menyiapkan variable penampung berupa array





            // $total[$u->mahasiswa_id] = 0;
            // //Normalisasi Alternatif
            // $normalisasi[$v->id][$u->mahasiswa_id] = $u->nilai / 100;



            // // perkalian bobo alternatif dan kriteria
            // foreach ($normalisasi as $k => $v) {

            //     foreach ($v as $i => $u) {
            //         $hasil[$k][$i] = $bobot[$k] * $nilaiutility;
            //         $total[$i] = $total[$i] +   $hasil[$k][$i];
            //         // echo $bobot[$k]." x ".$u." = ".$hasil[$k][$i];
            //         // echo "<br>";
            //     }
            // }
            // echo "<br>";


        }





        // return view('aspirasi/index', ['data_mahasiswa' => $data_mahasiswa], ['kriteria' => $kriteria]);

    }
}
