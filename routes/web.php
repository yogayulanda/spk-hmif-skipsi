<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('/auths//login');
});



// Login
Route::get('/login', 'AuthController@login')->name('login');
Route::post('/postlogin', 'AuthController@postlogin');
Route::get('/logout', 'AuthController@logout');

Route::group(['middleware' => 'auth'], function () {
    // Dashboard
    Route::get('/dashboard', 'DashboardController@index');



    // Mahasiswa
    Route::get('/mahasiswa', 'MahasiswaController@index');
    Route::POST('/mahasiswa/create', 'MahasiswaController@create');
    Route::get('/mahasiswa/{id}/edit', 'MahasiswaController@edit');
    Route::post('/mahasiswa/{id}/update', 'MahasiswaController@update');
    Route::get('/mahasiswa/{id}/delete', 'MahasiswaController@delete');
    Route::get('/mahasiswa/{id}/profile', 'MahasiswaController@profile');
    Route::post('/mahasiswa/{id}/addnilai', 'MahasiswaController@addnilai');
    Route::get('/mahasiswa/{id}/{kriteria_id}/deletenilai', 'MahasiswaController@deletenilai');

    // Divisi
    Route::get('/divisi', 'DivisiController@index');
    Route::POST('/divisi/create', 'DivisiController@create');
    Route::get('/divisi/{id}/edit', 'DivisiController@edit');
    Route::post('/divisi/{id}/update', 'DivisiController@update');
    Route::get('/divisi/{id}/delete', 'DivisiController@delete');

    // Aspirasi
    Route::get('/aspirasi', 'AspirasiController@index');
    Route::POST('/aspirasi/create', 'AspirasiController@create');
    Route::get('/aspirasi/{id}/edit', 'AspirasiController@edit');
    Route::post('/aspirasi/{id}/update', 'AspirasiController@update');
    Route::get('/aspirasi/{id}/delete', 'AspirasiController@delete');
    Route::get('/aspirasi/hitungutility', 'AspirasiController@hitungutility');
    Route::get('/aspirasi/smart', 'AspirasiController@smart');
    // Route::get('/aspirasi/{id}/inputnilai', 'AspirasiController@inputnilai');
    // Route::post('/aspirasi/{id}/save', 'AspirasiController@addnilai');
    // Route::get('/aspirasi/modalnilai', 'AspirasiController@modalnilai');
    // Route::post('/aspirasi/savenilai', 'AspirasiController@savenilai');
});
