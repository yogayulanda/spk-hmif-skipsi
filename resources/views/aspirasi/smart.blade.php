@extends('layout/master')

@section('content')
<!-- Utility Tabel -->
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="panel">
                        <div class="panel-heading">

                            <div class="right">
                                <a href="/aspirasi/smart" class="btn btn-info">Export PDF</a>
                                <a href="/aspirasi/" class="btn btn-danger">Back</a>
                            </div>
                            <h3 class="panel-title">Nilai Utility</h3>

                        </div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        @foreach($kriteria as $krt)
                                        <th>{{$krt->nama_kriteria}}</th>
                                        @endforeach
                                        <th>Hasil Perhitungan</th>
                                        <th>Keterangan</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data_mahasiswa as $p)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{ $p->nama_mahasiswa }}</td>
                                        @foreach($p->KriteriaAspirasi as $n)
                                        <td>{{$n->pivot->nilai}}</td>
                                        @endforeach
                                        @foreach($data_mahasiswa as $hsl)
                                        <td></td>
                                        @endforeach
                                        <td>Layak</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop