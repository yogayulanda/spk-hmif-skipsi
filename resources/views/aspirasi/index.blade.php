@extends('layout/master')

@section('content')
@if(session('sukses'))
<div class="alert alert-success" role="alert">
    {{(session('sukses'))}}
</div>
@endif
<!-- Kriteria Tabel -->
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="panel">
                        <div class="panel-heading">

                            <h3 class="panel-title">Kriteria Dan Bobot Divisi Aspirasi dan Advokasi</h3>
                            <div class="right">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal">
                                    <i class="fa fa-plus-square">
                                        Tambahka Kriteria
                                    </i>
                                </button>

                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kriteria</th>
                                        <th>Bobot</th>
                                        <th>Nilai Utility</th>
                                        <th>Aksi</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>

                                        @foreach($kriteria as $krt)
                                        <th>{{$loop->iteration}}</th>
                                        <td>{{$krt->nama_kriteria}}</td>
                                        <td>{{$krt->bobot_kriteria}}</td>
                                        <td>{{$krt->bobot_kriteria/100}}</td>


                                        <td>
                                            <a href="/aspirasi/{{$krt->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                            <a href="/aspirasi/{{$krt->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Mau Dihapus?')"><i class="fa fa-trash"> Delete</i></a>
                                        </td>


                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Kriteria -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambahkan Kriteria</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/aspirasi/create" method="POST">
                            {{csrf_field()}}
                            <div class="form-group has-error">
                                <label for="example">Nama Kriteria</label>
                                <input name="nama_kriteria" type="text" class="form-control" id="nama_mahasiswa" placeholder="Masukkan Nama Kriteria">
                            </div>
                            <div class="form-group">
                                <label for="example">Bobot</label>
                                <input name="bobot_kriteria" type="number" class="form-control" id="nim" placeholder="Masukkan Bobot Kriteria">
                            </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Mahasiswa Tabel -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="panel">
                        <div class="panel-heading">

                            <div class="right">
                                <a href="/aspirasi/hitungutility" class="btn btn-info">Nilai Utility</a>
                                <a href="/aspirasi/smart" class="btn btn-danger">Lakukan Perhitungan</a>
                            </div>
                            <h3 class="panel-title">Data Penilaian</h3>

                        </div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        @foreach($kriteria as $krt)
                                        <th>{{$krt->nama_kriteria}}</th>
                                        @endforeach


                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data_mahasiswa as $p)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{ $p->nama_mahasiswa }}</td>
                                        @foreach($p->KriteriaAspirasi as $n)
                                        <td>{{$n->pivot->nilai}}</td>
                                        @endforeach
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop