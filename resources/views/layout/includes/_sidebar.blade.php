    <div id="sidebar-nav" class="sidebar">
        <div class="sidebar-scroll">
            <nav>
                <ul class="nav">
                    <li><a href="/dashboard" class="active"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
                    <li><a href="/mahasiswa" class=""><i class="lnr lnr-user"></i> <span>Mahasiswa</span></a></li>
                    <li><a href="/divisi" class=""><i class="lnr lnr-user"></i> <span>Divisi</span></a></li>
                    <span>-------------------------------------------------------</span>
                    <p>Perhitungan Kenaikan Kader Per-Divisi</p>
                    <span>-------------------------------------------------------</span>
                    <li><a href="/aspirasi" class=""><i class="lnr lnr-user"></i> <span>Aspirasi</span></a></li>
                    <li><a href="/litbang" class=""><i class="lnr lnr-user"></i> <span>Litbang</span></a></li>
                    <li><a href="/humas" class=""><i class="lnr lnr-user"></i> <span>Humas</span></a></li>
                    <li><a href="/keorganisasian" class=""><i class="lnr lnr-user"></i> <span>Keorganisasian</span></a></li>
                    <br>
                    <span>-------------------------------------------------------</span>
                    <li><a href="/pengurus" class=""><i class="lnr lnr-user"></i> <span>Pengurus</span></a></li>
                </ul>
            </nav>
        </div>
    </div>