@extends('layout/master')

@section('content')
@if(session('sukses'))
<div class="alert alert-success" role="alert">
    {{(session('sukses'))}}
</div>
@endif
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="panel">
                        <div class="panel-heading">

                            <h3 class="panel-title">Data Kader HMIF</h3>



                            <!-- Modal Tambah Mahasiswa -->
                            <div class="right">

                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal">
                                    <i class="fa fa-plus-square">
                                        Tambah Data Kader
                                    </i>
                                </button>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Nim</th>
                                        <th>Kelas</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Nilai</th>
                                        <th>Keterangan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        @foreach($data_mahasiswa as $mahasiswa)
                                        <th>{{$loop->iteration}}</th>
                                        <td>{{$mahasiswa->nama_mahasiswa}}</td>
                                        <td>{{$mahasiswa->nim}}</td>
                                        <td>{{$mahasiswa->kelas}}</td>
                                        <td>{{$mahasiswa->jenis_kelamin}}</td>
                                        <td>{{$mahasiswa->hasil_kriteria}}</td>
                                        <td>{{$mahasiswa->ket_hasil}}</td>
                                        <td>
                                            <a href="/mahasiswa/{{$mahasiswa->id}}/profile" class="btn btn-success btn-sm">Input Nilai</a>
                                            <!-- MOdal Input Nilai -->
                                            <a href="/mahasiswa/{{$mahasiswa->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                            <a href="/mahasiswa/{{$mahasiswa->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin Mau Dihapus?')"><i class="fa fa-trash"> Delete</i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Kader</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/mahasiswa/create" method="POST">
                    {{csrf_field()}}
                    <div class="form-group has-error">
                        <label for="example">Nama Kader</label>
                        <input name="nama_mahasiswa" type="text" class="form-control" id="nama_mahasiswa" placeholder="Masukkan Nama">
                    </div>
                    <div class="form-group">
                        <label for="example">Nim</label>
                        <input name="nim" type="text" class="form-control" id="nim" placeholder="Masukkan NIM">
                    </div>
                    <div class="form-group">
                        <label for="example">Kelas</label>
                        <input name="kelas" type="text" class="form-control" id="kelas" placeholder="Masukkan Kelas">
                    </div>
                    <div class="form-group">
                        <label name='jenis_kelamin' for="exampleFormControlSelect">Jenis Kelamin</label>
                        <select name='jenis_kelamin' class="custom-select">
                            <option selected>Piih Jenis Kelamin</option>
                            <option value="Laki-Laki">Laki-Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Nilai</label>
                        <input name='hasil_kriteria' type="text" class="form-control" id="hasil_kriteria" placeholder="Masukkan nilai ">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Keterangan</label>
                        <input name='ket_hasil' type="text" class="form-control" id="ket_hasil" placeholder="Keterangan Hasil">
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@stop