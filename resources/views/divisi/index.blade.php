@extends('layout/master')

@section('content')
@if(session('sukses'))
<div class="alert alert-success" role="alert">
    {{(session('sukses'))}}
</div>
@endif
<div class="main">
    <div class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="panel">
                        <div class="panel-heading">

                            <h3 class="panel-title">Data Divisi</h3>
                            <div class="right">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal">
                                    <i class="fa fa-plus-square">
                                        Tambah Divisi
                                    </i>
                                </button>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Kepala Divisi</th>
                                        <th>Aksi</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        @foreach($divisi as $divisi)
                                        <th>{{$loop->iteration}}</th>
                                        <td>{{$divisi->nama_divisi}}</td>
                                        <td>{{$divisi->kepala_divisi}}</td>

                                        <td>
                                            <a href="/divisi/{{$divisi->id}}/edit" class="btn btn-warning btn-sm">Edit</a>

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Divisi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/divisi/create" method="POST">
                    {{csrf_field()}}
                    <div class="form-group has-error">
                        <label for="example">Nama Divisi</label>
                        <input name="nama_divisi" type="text" class="form-control" id="nama_divisi" placeholder="Masukkan Nama">
                    </div>
                    <div class="form-group">
                        <label for="example">Nama Kepala Divisi</label>
                        <input name="kepala_divisi" type="text" class="form-control" id="nim" placeholder="Masukkan NIM">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@stop